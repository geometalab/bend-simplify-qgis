# bend-simplify-py3
Wang-Mueller Bend Simplify algorithm in python 3

## How to install
* Download the folder 'bend_simplify' for example as a zip-file
* Extract the folder and put it in 'YOUR_PATH/QGIS/QGIS3/profiles/YOUR_PROFILE/python/plugins'
* Run QGIS 3.X.X
* Go to 'Plugins' -> 'Manage and Install Plugins...'
* Enable 'Bend Simplify'
* In your Toolbox, there should now be 'Bend Simplify' -> 'Simplify with Bend Simplify'

Visit the [wiki-page](https://gitlab.com/geometalab/bend-simplify-qgis/-/wikis/home) for more information.