import math

from qgis.core import QgsGeometry, QgsPointXY, QgsWkbTypes

from .bezier import bezier


def simplify_line(line, parameters):
    """Calls the bend_simplify method for each lineString in a given multiLineString
    or for one given lineString."""
    if line.wkbType() == QgsWkbTypes.MultiLineString:
        simplified_line = QgsGeometry.unaryUnion(
            [_bend_simplify(part, *parameters) for part in line.asGeometryCollection()]
        )
    else:
        simplified_line = _bend_simplify(line, *parameters)
    return simplified_line


def _bend_simplify(
    line,
    max_distance_of_vertices,
    no_vertices_to_split_a_curve,
    remove_small_polygons=False,
):
    """Simplifies a line by using the bend simplify algorithm with Bézier"""
    points = [list(point) for point in line.asPolyline()]  # convert linestring to list
    points = _cleanup_line(points)
    point_counter = 1
    start = 0
    point_amount = len(points)
    distance_total = 0
    middle_point_list = []
    new_point_list = [points[0]]

    # loop through each of the points
    for point in points:
        if point_counter < point_amount:
            # compare points distance
            distance = _linear_distance(points[start], points[point_counter])
            # collect points if distance_total is less than max_distance_of_vertices
            if (distance + distance_total) <= max_distance_of_vertices:
                distance_total += distance
                middle_point_list.append(points[point_counter])
            elif point_counter - start <= 1:
                start = point_counter
                middle_point_list = []
            else:
                # calculate bezier_curve
                triangle_area = 0
                bezier_middle_point = point
                for mid_point in middle_point_list:
                    a = distance
                    b = _linear_distance(points[start], mid_point)
                    c = _linear_distance(mid_point, points[point_counter])
                    s = 0.5 * (a + b + c)
                    area = math.sqrt(s * (s - a) * (s - b) * (s - c))
                    if area > triangle_area:
                        # save midpoint for bezier
                        bezier_middle_point = mid_point
                        triangle_area = area

                middle_point_list = []
                x_list = [
                    points[start][0],
                    bezier_middle_point[0],
                    points[point_counter][0],
                ]
                y_list = [
                    points[start][1],
                    bezier_middle_point[1],
                    points[point_counter][1],
                ]
                start = point_counter
                bezier_curve = bezier(x_list, y_list, no_vertices_to_split_a_curve)

                new_point_list += bezier_curve
                distance_total = 0
                del bezier_curve, x_list, y_list, bezier_middle_point
            point_counter += 1

        else:
            new_point_list += [points[point_counter - 1]]
    new_point_list = _cleanup_line(new_point_list)

    if not remove_small_polygons:
        if new_point_list[0] == new_point_list[-1]:
            if len(new_point_list) <= 3:
                return line
    return QgsGeometry.fromPolylineXY(
        [QgsPointXY(point[0], point[1]) for point in new_point_list]
    )


def _cleanup_line(point_list):
    """Deletes points from a point list so that
    there are no two identical points next to each
    other anymore."""
    clean_point_list = []
    for point in point_list:
        if len(clean_point_list) != 0:
            if point == clean_point_list[-1]:
                continue
        clean_point_list.append(point)
    return clean_point_list


def _linear_distance(p1, p2):
    """Calculates the linear distance between two points in a 2 dimensional space."""
    a = p1[0] - p2[0]
    b = p1[1] - p2[1]
    return math.sqrt(a ** 2 + b ** 2)
