class PostIncrement:
    def __init__(self, starting_number, increment=1):
        self.i = starting_number
        self.increment = increment

    def next(self):
        """Returns the current number, then it gets increased."""
        i = self.i
        self.i += self.increment
        return i

    def current(self):
        """Returns the current number."""
        return self.i

    def set_value(self, number):
        """Gives the number a new value."""
        self.i = number
