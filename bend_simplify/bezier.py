def _binom(n, k):
    """Calculates the binomial coefficient from two given numbers"""
    if not 0 <= k <= n:
        return 0
    b = 1
    for t in range(min(k, n - k)):
        b *= n
        b /= t + 1
        n -= 1
    return b


def _linspace(num):
    """Creates a list containing numbers ranging from 0 to 1.
    The amount of numbers is defined by the given parameter."""
    difference = 1 / (num - 1)
    return [i * difference for i in range(num)]


def _zeros(num):
    """Creates a list with lists which contain two zeros each."""
    return [[0.0, 0.0] for i in range(num)]


def _outer(numbers, point):
    """Creates a list of points for each number in the given list.
    The coordinates of the point is the given point multiplied by its number."""
    return [[number * point[0], number * point[1]] for number in numbers]


def _bernstein(n, k):
    """Bernstein polynomial."""
    coefficient = _binom(n, k)

    def _bpoly(x):
        a_list = [(1 - number) ** (n - k) for number in x]
        return [coefficient * number ** k * a_list[i] for i, number in enumerate(x)]

    return _bpoly


def bezier(x_list, y_list, num):
    """Build Bézier curve from points."""
    points = list(zip(x_list, y_list))
    N = len(points)
    t = _linspace(num)
    curve = _zeros(num)
    for ii in range(N):
        outer_line = _outer(_bernstein(N - 1, ii)(t), points[ii])
        for i in range(len(curve)):
            curve[i][0] += outer_line[i][0]
            curve[i][1] += outer_line[i][1]
    return curve
