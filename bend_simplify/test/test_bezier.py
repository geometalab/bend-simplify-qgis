from bezier import _bernstein, _binom, _linspace, _outer, _zeros, bezier


def test_binom():
    assert _binom(6, 4) == 15


def test_linspace():
    assert _linspace(5) == [0.0, 0.25, 0.5, 0.75, 1.0]


def test_zeros():
    assert _zeros(5) == [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]


def test_outer():
    par1 = [1.0, 0.25, 0.0]
    par2 = (-5.049081138472623, 5.705252456063761)
    assert _outer(par1, par2) == [
        [-5.049081138472623, 5.705252456063761],
        [-1.2622702846181557, 1.4263131140159402],
        [-0.0, 0.0],
    ]


def test_bernstein():
    assert _bernstein(4, 3)(_linspace(5)) == [0.0, 0.046875, 0.25, 0.421875, 0.0]


def test_bezier():
    assert bezier(
        [-5.04908114, 12.54798229, 7.69361997], [5.70525246, 11.57094027, 6.10978265], 3
    ) == [
        [-5.04908114, 5.70525246],
        [6.935125852500001, 8.7392289125],
        [7.69361997, 6.10978265],
    ]
