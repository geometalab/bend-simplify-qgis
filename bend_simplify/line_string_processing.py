from qgis.core import QgsGeometry, QgsTask, QgsWkbTypes

from .geometry_processing import *


def get_line_intersections(lines, feedback, total):
    """Iterates through a list of line strings and finds every intersection"""
    feedback.setProgressText("Find all intersections")
    for i, line_string_i in lines.items():
        for j, line_string_j in lines.items():
            if j > i and line_string_i.boundingBoxIntersects(line_string_j):
                overlap = None
                remains_i, remains_j = line_string_i, line_string_j
                intersection = line_string_i.intersection(line_string_j)
                if not intersection.isEmpty():
                    if intersection.wkbType() == QgsWkbTypes.GeometryCollection:
                        geom_collection = intersection.asGeometryCollection()
                        lines_list = []
                        for geometry in geom_collection:
                            if geometry.wkbType() == QgsWkbTypes.LineString:
                                lines_list.append(geometry.asPolyline())
                        intersection = QgsGeometry.fromMultiPolylineXY(lines_list)
                    if intersection.wkbType() == QgsWkbTypes.MultiLineString:
                        geom_collection = intersection.asGeometryCollection()
                        intersection = geom_collection[0]
                        for geometry in geom_collection[1:]:
                            if geometry.wkbType() == QgsWkbTypes.LineString:
                                intersection = intersection.combine(geometry)
                    if intersection.wkbType() in [
                        QgsWkbTypes.MultiLineString,
                        QgsWkbTypes.LineString,
                    ]:
                        overlap = intersection
                        remains_i = line_string_i.difference(overlap)
                        remains_j = line_string_j.difference(overlap)
                split_i = _split(remains_i, remains_j)
                split_j = _split(remains_j, remains_i)
                _merge_multilines(lines, split_i, overlap, i)
                _merge_multilines(lines, split_j, overlap, j)
        feedback.setProgress(int(i * total))


def _split(line_a, line_b):
    """Splits line_a at every location where it touches line_b."""
    copy_a = (
        QgsGeometry.unaryUnion(line_a.asGeometryCollection())
        if line_a.wkbType() == QgsWkbTypes.MultiLineString
        else QgsGeometry.fromPolylineXY(line_a.asPolyline())
    )
    points_b = []
    if line_b.wkbType() == QgsWkbTypes.MultiLineString:
        for geometry in line_b.asGeometryCollection():
            points_b.append(geometry.asPolyline())
    else:
        points_b.append(line_b.asPolyline())
    for point_list in points_b:
        _, remains, __ = copy_a.splitGeometry(point_list, False)
        if len(remains) != 0:
            copy_a = QgsGeometry.unaryUnion([copy_a] + remains)
    return copy_a


def _merge_multilines(lines, line_a, line_b, index):
    """Creates a multi line string from two multi lines or linestrings and adds it to the geometry list"""
    if line_b is not None:
        lines[index] = QgsGeometry.unaryUnion([line_a, line_b])
    else:
        lines[index] = line_a


class SimplifiedLinesTask(QgsTask):
    def __init__(
        self, lines, max_distance_of_vertices, no_vertices_to_split_a_curve,
    ):
        QgsTask.__init__(self)
        self.lines = lines
        self.max_distance_of_vertices = max_distance_of_vertices
        self.no_vertices_to_split_a_curve = no_vertices_to_split_a_curve

        self.simplified_lines = []

        self.finishedTask = False
        self.error = False

    def run(self):
        """Simplifies all the lines and merges them at the places
        they were split before."""
        try:
            parameters = [
                self.max_distance_of_vertices,
                self.no_vertices_to_split_a_curve,
            ]
            for line_string in self.lines.values():
                simplified_multi_line = simplify_line(
                    line_string, parameters
                ).asGeometryCollection()
                merged_simplified_line = simplified_multi_line[0]
                for line in simplified_multi_line[1:]:
                    merged_simplified_line = merged_simplified_line.combine(line)
                self.simplified_lines.append(merged_simplified_line)
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True
