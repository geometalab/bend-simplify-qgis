from qgis.core import QgsGeometry, QgsTask, QgsWkbTypes

from .geometry_processing import *
from .post_increment import PostIncrement


def get_polygon_lists(polygons, feedback, total):
    """Creates five lists, which contain different kinds of polygons"""
    feedback.setProgressText("Extract all polygons")
    idx = PostIncrement(0)
    structured_polygon_list = (
        []
    )  # every polygon / multipolygon adds its "structure" to this list.
    simple_polygon_dict = (
        {}
    )  # every simple polygon and filled ring-polygons will be added to this dictionary.
    ring_polygon_list = (
        []
    )  # every ring-polygon in its original shape gets added to this list.
    interior_polygon_dict = (
        {}
    )  # every interior of ring-polygons gets added to this dictionary, as a polygon.
    exclaves_dict = (
        {}
    )  # every polygon, that lies within another, gets added to this dictionary.
    for current, multiPolygon in polygons.items():
        if feedback.isCanceled():
            break
        multi_polygon_parts = []
        for polygon in multiPolygon:
            ring_list = polygon.asPolygon()
            if len(ring_list) > 1:
                exterior_index = idx.next()
                simple_polygon_dict[exterior_index] = polygon.removeInteriorRings()
                ring_polygon_list.append(polygon)
                interior_index_list = []
                for interior in ring_list[1:]:
                    interior_index = idx.next()
                    interior_polygon_dict[interior_index] = QgsGeometry.fromPolygonXY(
                        [interior]
                    )
                    interior_index_list.append(interior_index)
                multi_polygon_parts.append((exterior_index, interior_index_list))
            else:
                polygon_index = idx.next()
                simple_polygon_dict[polygon_index] = polygon
                multi_polygon_parts.append(polygon_index)
        structured_polygon_list.append(multi_polygon_parts)

        feedback.setProgress(int(current * total))

    feedback.setProgressText("Extract all exclaves")
    idx.set_value(0)
    polygon_count = len(simple_polygon_dict)
    total = 100.0 / polygon_count if polygon_count else 0

    # Find every polygon, that is an exclave, and remove it from the simple polygon dictionary
    for i, polygon_i in simple_polygon_dict.items():
        if feedback.isCanceled():
            break
        for j, polygon_j in simple_polygon_dict.items():
            if i != j and polygon_i.boundingBoxIntersects(polygon_j):
                if polygon_i.within(polygon_j):
                    exclaves_dict[i] = polygon_i
                    break
        feedback.setProgress(int(idx.next() * total))
    for key in exclaves_dict.keys():
        del simple_polygon_dict[key]

    return (
        structured_polygon_list,
        simple_polygon_dict,
        ring_polygon_list,
        interior_polygon_dict,
        exclaves_dict,
    )


class OuterIntersectionsTask(QgsTask):
    def __init__(self, simple_polygon_dict):
        QgsTask.__init__(self)
        self.simple_polygon_dict = simple_polygon_dict

        self.intersection_dict = {}
        self.intersection_index_dict = {k: [] for k in self.simple_polygon_dict.keys()}

        self.finishedTask = False
        self.error = False

    def run(self):
        """Finds every border that is shared by two outer rings from polygons."""
        try:
            idx = PostIncrement(0, 2)
            for i, polygon_i in self.simple_polygon_dict.items():
                for j, polygon_j in self.simple_polygon_dict.items():
                    if j > i and polygon_i.boundingBoxIntersects(polygon_j):
                        _extract_intersection(
                            self.intersection_dict,
                            self.intersection_index_dict,
                            polygon_i.intersection(polygon_j),
                            i,
                            j,
                            idx,
                        )
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True


class InnerIntersectionsTask(QgsTask):
    def __init__(self, ring_polygon_list, interior_polygon_dict, exclaves_dict):
        QgsTask.__init__(self)
        self.ring_polygon_list = ring_polygon_list
        self.interior_polygon_dict = interior_polygon_dict
        self.exclaves_dict = exclaves_dict

        self.intersection_dict = {}
        all_inner_polygons = {**self.interior_polygon_dict, **self.exclaves_dict}
        self.intersection_index_dict = {k: [] for k in all_inner_polygons.keys()}

        self.finishedTask = False
        self.error = False

    def run(self):
        """Finds every border from enclaves that are shared with another
        enclave or its surrounding polygon."""
        try:
            idx = PostIncrement(1, 2)
            for ex_key_i, exclave_i in self.exclaves_dict.items():
                found_equal = False
                for in_key, interior in self.interior_polygon_dict.items():
                    if exclave_i.boundingBoxIntersects(interior):
                        if exclave_i.isGeosEqual(interior):
                            _append_border(
                                self.intersection_dict,
                                self.intersection_index_dict,
                                QgsGeometry.fromPolylineXY(exclave_i.asPolygon()[0]),
                                ex_key_i,
                                in_key,
                                idx.next(),
                            )
                            found_equal = True
                            break
                        elif exclave_i.within(interior):
                            found_within = False
                            for ring_polygon in self.ring_polygon_list:
                                if exclave_i.boundingBoxIntersects(ring_polygon):
                                    found_within = _extract_intersection(
                                        self.intersection_dict,
                                        self.intersection_index_dict,
                                        exclave_i.intersection(ring_polygon),
                                        ex_key_i,
                                        in_key,
                                        idx,
                                    )
                                    if found_within:
                                        break
                            if found_within:
                                break
                if found_equal:
                    continue
                for ex_key_j, exclave_j in self.exclaves_dict.items():
                    if ex_key_j > ex_key_i and exclave_i.boundingBoxIntersects(
                        exclave_j
                    ):
                        _extract_intersection(
                            self.intersection_dict,
                            self.intersection_index_dict,
                            exclave_i.intersection(exclave_j),
                            ex_key_i,
                            ex_key_j,
                            idx,
                        )
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True


def _extract_intersection(
    intersection_dict, intersection_index_dict, intersection, i, j, idx
):
    """Extracts the common border of two polygons and adds the border to a list"""
    if not intersection.isEmpty():
        if intersection.wkbType() in [
            QgsWkbTypes.MultiLineString,
            QgsWkbTypes.LineString,
        ]:
            _append_border(
                intersection_dict,
                intersection_index_dict,
                intersection,
                i,
                j,
                idx.next(),
            )
            return True
        if intersection.wkbType() == QgsWkbTypes.GeometryCollection:
            line_string_list = []
            geom_collection = intersection.asGeometryCollection()
            for geometry in geom_collection:
                if geometry.wkbType() == QgsWkbTypes.LineString:
                    line_string_list.append(geometry)
            _append_border(
                intersection_dict,
                intersection_index_dict,
                QgsGeometry.unaryUnion(line_string_list),
                i,
                j,
                idx.next(),
            )
            return True
    return False


def _append_border(
    intersection_dict, intersection_index_dict, intersection, i, j, index
):
    """Adds a shared border to a list"""
    if intersection.wkbType() == QgsWkbTypes.MultiLineString:
        line_string_list = intersection.asGeometryCollection()
        intersection = line_string_list[0]
        for line_string in line_string_list[1:]:
            intersection = intersection.combine(line_string)

    intersection_dict[index] = intersection
    intersection_index_dict[i].append(index)
    intersection_index_dict[j].append(index)


class GetPolygonRemainsTask(QgsTask):
    def __init__(self, intersection_index_dict, intersection_dict, all_polygons_dict):
        QgsTask.__init__(self)
        self.intersection_index_dict = intersection_index_dict
        self.intersection_dict = intersection_dict
        self.all_polygons_dict = all_polygons_dict

        self.finishedTask = False
        self.error = False

    def run(self):
        """Every polygon gets subtracted by all its borders it shares
        with others. What remains is added to the intersection_indexes list."""
        try:
            for (
                polygon_index,
                intersection_indexes,
            ) in self.intersection_index_dict.items():
                remains = QgsGeometry.fromPolylineXY(
                    self.all_polygons_dict[polygon_index].asPolygon()[0]
                )
                for intersection_index in intersection_indexes:
                    remains = remains.difference(
                        self.intersection_dict[intersection_index]
                    )
                if not remains.isEmpty():
                    intersection_indexes.append(remains)
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True


class SimplifiedIntersectionsTask(QgsTask):
    def __init__(
        self,
        intersection_dict,
        max_distance_of_vertices,
        no_vertices_to_split_a_curve,
        remove_small_polygons,
    ):
        QgsTask.__init__(self)
        self.intersection_dict = intersection_dict
        self.max_distance_of_vertices = max_distance_of_vertices
        self.no_vertices_to_split_a_curve = no_vertices_to_split_a_curve
        self.remove_small_polygons = remove_small_polygons

        self.finishedTask = False
        self.error = False

    def run(self):
        """Simplifies every intersection."""
        try:
            parameters = [
                self.max_distance_of_vertices,
                self.no_vertices_to_split_a_curve,
                self.remove_small_polygons,
            ]
            for intersection_index, intersection in self.intersection_dict.items():
                simplified_line = simplify_line(intersection, parameters)
                self.intersection_dict[intersection_index] = simplified_line
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True


class SimplifiedRemainsTask(QgsTask):
    def __init__(
        self,
        intersection_index_dict,
        max_distance_of_vertices,
        no_vertices_to_split_a_curve,
        remove_small_polygons,
    ):
        QgsTask.__init__(self)
        self.intersection_index_dict = intersection_index_dict
        self.max_distance_of_vertices = max_distance_of_vertices
        self.no_vertices_to_split_a_curve = no_vertices_to_split_a_curve
        self.remove_small_polygons = remove_small_polygons

        self.finishedTask = False
        self.error = False

    def run(self):
        """Simplifies all the remaining lines."""
        try:
            parameters = [
                self.max_distance_of_vertices,
                self.no_vertices_to_split_a_curve,
                self.remove_small_polygons,
            ]
            for intersection_indexes in self.intersection_index_dict.values():
                if isinstance(intersection_indexes[-1], QgsGeometry):
                    remains = intersection_indexes[-1]
                    simplified_line = simplify_line(remains, parameters)
                    intersection_indexes[-1] = simplified_line
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True


class BuildPolygonTask(QgsTask):
    def __init__(
        self,
        structured_polygon_list,
        simplified_lines_dict,
        intersection_index_dict,
        remove_small_polygons,
    ):
        QgsTask.__init__(self)
        self.structured_polygon_list = structured_polygon_list
        self.simplified_lines_dict = simplified_lines_dict
        self.intersection_index_dict = intersection_index_dict
        self.remove_small_polygons = remove_small_polygons

        self.simplified_polygon_list = []

        self.finishedTask = False
        self.error = False

    def run(self):
        """Rebuilds every polygon with simplified lines"""
        try:
            for multi_polygon in self.structured_polygon_list:
                multi_polygon_parts = []
                for polygon in multi_polygon:
                    if isinstance(polygon, tuple):
                        exterior = _build_polygon_part(
                            polygon[0],
                            self.simplified_lines_dict,
                            self.intersection_index_dict,
                        )
                        exterior_polygon = QgsGeometry.fromPolygonXY(
                            [exterior.asPolyline()]
                        )
                        if not exterior.isEmpty() and not (
                            self.remove_small_polygons and exterior_polygon.area() == 0
                        ):
                            interiors = []
                            for index in polygon[1]:
                                interior = _build_polygon_part(
                                    index,
                                    self.simplified_lines_dict,
                                    self.intersection_index_dict,
                                )
                                interior_polygon = QgsGeometry.fromPolygonXY(
                                    [interior.asPolyline()]
                                )
                                if (
                                    not interior.isEmpty()
                                    and not (
                                        self.remove_small_polygons
                                        and interior_polygon.area() == 0
                                    )
                                    and interior_polygon.intersects(exterior_polygon)
                                ):
                                    interiors.append(interior.asPolyline())
                            part = QgsGeometry.fromPolygonXY(
                                [exterior.asPolyline()] + interiors
                            )
                            multi_polygon_parts.append(part)
                    else:
                        polygon = _build_polygon_part(
                            polygon,
                            self.simplified_lines_dict,
                            self.intersection_index_dict,
                        )
                        polygon = QgsGeometry.fromPolygonXY([polygon.asPolyline()])
                        if not polygon.isEmpty() and not (
                            self.remove_small_polygons and polygon.area() == 0
                        ):
                            multi_polygon_parts.append(polygon)
                self.simplified_polygon_list.append(
                    QgsGeometry.unaryUnion(multi_polygon_parts)
                )
        except:
            self.error = True
            return False

        self.finishedTask = True
        return True


def _build_polygon_part(index, simplified_lines_dict, intersection_index_dict):
    """Rebuilds one linear ring, which is a part of a multi polygon"""
    intersection_indexes = intersection_index_dict[index]
    lines_list = []
    for intersection_index in intersection_indexes:
        if isinstance(intersection_index, int):
            line = simplified_lines_dict[intersection_index]
        else:
            line = intersection_index
        if line.wkbType() == QgsWkbTypes.MultiLineString:
            for part in line.asGeometryCollection():
                lines_list.append(part)
        else:
            lines_list.append(line)
    ring = lines_list[0]
    for line_string in lines_list[1:]:
        ring = ring.combine(line_string)
    return ring
